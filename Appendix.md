# Appendix A - Mali Gxx reference

Mali Gxx covers both Bifrost (major 6, 7) and Valhall (major 9). Architecture
major 8 did not ship in any products. It was likely an intermediate step in
between Bifrost and Valhall, perhaps implementing the Bifrost instruction set
with an early version of Valhall's data structures.

Name       Codename  Major   Minor
---------- --------- ------- ------
Mali G71   Mimir     6       0
Mali G72   Heimdall  6       1
Mali G51   Sigurd    7       0
Mali G76   Norr      7       1
Mali G52   Gondul    7       2
Mali G31   Dvalin    7       3
Mali G77   Trym      9       0
Mali G57   Natt-A    9       1
Mali G78   Borr      9       2
Mali G57   Natt-B    9       3
Mali G68   Ottr      9       4
Mali G78AE Borr-AE   9       5

# Appendix B - Performance characteristics

Mali G78 attains the following normalized peak performance, as reported by the
Mali Offline Compiler:

* 64 FMA instructions per cycle
* 64 CVT instructions per cycle
* 16 SFU instructions per cycle
* 8 x 32-bit varying channels interpolated per cycle
* 4 texture instructions per cycle
* 1 load/store operation per cycle

64-bit instructions operate at half-speed compared to 32-bit instructions. For
example, at most 32 `SHADDX.u64` instructions may be executed per normalized
cycle, since `SHADDX.u64` runs on the CVT unit.

Depending on the cache hit rate, instructions accessing memory can be
significantly slower than reported here. Depending on the exact texture
instruction issued and the corresponding sampler state, texture instructions
can be additionally slower.
