all: Overview.md Appendix.md docs.md
	cat Overview.md docs.md Appendix.md > /dev/shm/files.md
	pandoc /dev/shm/files.md -o Valhall-Documentation.pdf --pdf-engine=lualatex


docs.md: docs.py ISA.xml
	python3 docs.py > docs.md
