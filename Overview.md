---
header-includes:
 - \usepackage{graphicx}
 - \usepackage[margin=1.15in]{geometry}
 - \usepackage{fontspec}
 - \setmainfont[Mapping=tex-text]{Liberation Serif}
 - \setsansfont[Mapping=tex-text]{Karla}
 - \setmonofont[Mapping=tex-text]{Liberation Mono}
...

\begin{titlepage}
\sffamily
\begin{center}
\vspace*{1cm}
\Huge
\textbf{Valhall Instruction Set Reference}
\vfill
\includegraphics[width=0.4\textwidth]{Logo}
\end{center}
\end{titlepage}

\pagebreak

Portions of this work derive from `ISA.xml`, which is SPDX-MIT:

\footnotesize
> Copyright \copyright 2021 Collabora Ltd.
>
> Permission is hereby granted, free of charge, to any person obtaining a
> copy of this software and associated documentation files (the "Software"),
> to deal in the Software without restriction, including without limitation
> the rights to use, copy, modify, merge, publish, distribute, sublicense,
> and/or sell copies of the Software, and to permit persons to whom the
> Software is furnished to do so, subject to the following conditions:
>
> The above copyright notice and this permission notice (including the next
> paragraph) shall be included in all copies or substantial portions of the
> Software.
>
> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
> IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
> FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
> THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
> LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
> OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
> SOFTWARE.

\normalsize

Arm Limited has registered trademarks and uses trademarks. For a list of trademarks of Arm, please see their Trademark list page at <https://www.arm.com/company/policies/trademarks/arm-trademark-list>. Arm\textregistered is a registered trademark of Arm. Mali\texttrademark is a trademark of Arm.

This work was produced by Collabora, Ltd. independent of Arm with no affiliation.

The work is provided "as is", without warranty of any kind, express or
implied, including but not limited to the warranties of merchantability,
fitness for a particular purpose and noninfringement.  in no event shall
the authors or copyright holders be liable for any claim, damages or other
liability, whether in an action of contract, tort or otherwise, arising from,
out of or in connection with the software or the use or other dealings in the
software.

\vfill

Copyright \copyright 2021 Collabora, Ltd. \par
This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License (SPDX-CC-BY-SA-4.0). To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

\includegraphics{by-sa.png}

\pagebreak

# Change log

2021-07-20: Initial publication.

2021-07-21: Document restrictions on uniforms/constants. Correct various instructions.

2021-07-27: Document restriction on staging registers. Correct various instructions.

2021-08-02: Document execution units for each instruction and update performance characteristics.

2021-08-04: Document IEEE 754 rules for clamps. Document MUX.i32 modes.

2021-12-12: Document texture resource descriptor.

2022-01-27: Document resource access instructions in more depth.

\pagebreak

# Intro

**Valhall** is the instruction set used in the latest generation of Arm Mali GPUs, including:

Name       Codename  Major   Minor
---------- --------- ------- ------
Mali G77   Trym      9       0
Mali G57   Natt-A    9       1
Mali G78   Borr      9       2
Mali G57   Natt-B    9       3
Mali G68   Ottr      9       4
Mali G78AE Borr-AE   9       5

# Overview

Valhall is a _linearization_ of Bifrost, its predecessor instruction set. Recall Bifrost has instructions paired in tuples, tuples grouped in clauses, and machine state specified in the header of each clause. In Bifrost, it is the compiler's responsibility to group instructions as part of scheduling. This simplifies the hardware, at the expense of a much more complicated compiler and worse utilization of hardware resources on real workloads.

Valhall replaces Bifrost's static scheduling with a dynamic hardware scheduler, making it a superscalar architecture. At the instruction level, it is a simple evolution of Bifrost.

Valhall retains Bifrost's SIMD-within-a-word semantics. The hardware natively supports 8-bit, 16-bit, and 32-bit operations. While 32-bit operations are scalar, 16-bit operations are vectorized in pairs, and 8-bit operations are vectorized in quads. In theory, this permits 16-bit operations to have doubled throughput compared to 32-bit operations.

Valhall improves on Bifrost's 16-bit support by properly supporting write masks
on most 16-bit destinations, so programs using 16-bit operations get register
pressure halved even when vectorization is impossible. Additionally, write
masks on 32-bit floating point operations have the semantic of converting the
result to 16-bit.

# Dependencies

Valhall executes new instructions while prior high-latency instructions are
still in flight. This allows the architecture to hide the latency of memory
accesses, by issuing multiple memory accesses together or executing unrelated
arithmetic while the results are pending. When the results of a memory access
are required, the compiler must explicitly wait on the previous instruction,
inserting a dependency.

Recall on Bifrost, every clause is assigned one of eight "dependency slots",
and every clause can wait on any subset of these slots. Rather than specifying
in every instruction, Bifrost specifies this information in the clause header,
as a compromise between precision and code size.

Valhall has no clauses (and hence no clause headers), so it must specify this
information in the instructions themselves. This becomes practical by making
two simplifications to Bifrost:

First, general instructions only need to specify the dependencies. Arithmetic
instructions do not need a dependency slot assigned. The few instructions that
_do_ need dependency slots can specify their slot in another part of the
instruction.

Second, Valhall uses only 4 slots instead of 8, saving 4-bits for every
instruction. This simplication is practical, as the performance gain from
adding extra slots levels off quickly.

Obscurely, instructions that store to memory also require dependency slots
assigned. Waiting on these slots allows subgroup memory barriers to be
implemented inexpensively.

# Branching

Valhall has a single general-purpose branch instruction, branching to a
relative offset (in instructions) if its source is nonzero. Higher level
control flow is created by chained together branches with comparison
instructions. This simplification represents a departure from Bifrost, which
specified conditions within the many branch instructions.

Valhall is a warp-based architecture, grouping 16 threads into warps.
Divergence of threads within a warp carries a performance penalty. Divergence
is handled in hardware, but the compiler must insert some hints to ensure
divergence is handled correctly. Namely, the `.reconverge` action is required
on any instruction whose successor may be executed with a different execution
mask than it. That includes all divergent branches, as well as the last
instruction of blocks with divergent fallthrough.

Indirect access to attributes and texture handles must not be divergent. If
divergent access is required, the compiler must lower to an if-chain predicated
on lane ID:

```c
value = 0;
if (lane == 0)
   value = load()
else if (lane == 1)
   value = load()
...
else if (lane == MAX_LANE)
   value = load()
```

As Valhall warps are 16 threads, this requires about 100 instructions! Indirect
access should be avoided on Valhall, unless the compiler can prove the index is
not divergent.

# Texture instructions

Texture instructions are unique in their register requirements; depending on
the specific texturing operation, up to a dozen registers can be used on some
GPUs. As Valhall is limited in instruction size, there is no room to encode
each source register separately. Instead, the _base_ register is specified in
the instruction, and sources are found in the subsequent registers in the
following order:

1. X coordinate (floating-point)
1. Y coordinate (floating-point)
1. Z coordinate (floating-point)
1. Shadow comparison value (floating-point)
1. Texel offset (as a packed 8-bit vector, X in the bottom byte, Y in the second byte)
1. Explicit level-of-detail (as a 16-bit fixed-point, encoding 8:8)
1. Level-of-detail bias (as a 16-bit fixed-point, encoding 8:8)

Shading languages require the use of helper threads, which contribute to
screen-space derivative calculations (including the level-of-detail selection
in texture instructions), but do not correspond to rasterized pixels. Usually,
helper threads do not need to execute texture instructions once the
level-of-detail has been selected. Skipping texturing on helper threads can
save memory bandwidth. Valhall skips texturing for helper threads if the
`.skip` bit is set. `.skip` should be set if the results of texturing do not
contribute to future texture, derivative, or subgroup operations as determined
by data flow analysis. Once helper invocations are no longer required, the
`.td` action should be used to terminate the execution of the unused threads.

Texture projection is not supported by Valhall. Lower to `FRCP.f32` and
`FMA.f32` and then do the non-projection texture operation.

## Texture resource descriptor

Due to the fixed encoding, there is not space in the texture instruction to
encode both texture and sampler indices as either immediates or registers.
Instead, this information is encoded in a texture resource descriptor, packed
into a single 64-bit word as follows, and passed as the first argument of
texture access instructions.

Table: Texture Resource Descriptor

Bits      Value
--------  ------
0-10      Sampler index
11-15     Sampler table
16-26     Texture index
27-31     Texture table
32-35     Offset X
36-39     Offset Y

The sampler and texture descriptors are respectively found at the given index
of the respective table passed in the GPU data structures.

The offsets allow fixing a small immediate nonnegative texel offset, useful for
nonnegative arguments to `textureOffset`. Negative offsets must be handled with
an explicit offset passed via a staging register; this is slightly less
efficient.

Typically, this descriptor is packed at compile-time and passed as a
fast-access uniform.

## Cube maps

Before texturing from a cube map, the coordinates must be transformed using the Valhall instructions `CUBE_SSEL`, `CUBE_TSEL`, `CUBEFACE1`, and `CUBEFACE2`. These act as their Bifrost counterparts:

* `CUBEFACE1` takes the X, Y, and Z coordinates, and computes `max { |X|, |Y|, |Z| }`.
* `CUBEFACE2` takes the X, Y, and Z coordinates, and selects the cube face
* `CUBE_SSEL` takes the Z and X coordinates and the cube face, and selects the S coordinate.
* `CUBE_TSEL` takes the Y and Z coordinates and the cube face, and selects the T coordinate.

These operations are used as building blocks for the full cube face transformation. OpenGL specifies that an input vector $(x, y, z)$ is transformed to the vector

$$\begin{bmatrix}
\frac{1}{2} \left(\frac{s}{\text{max} \; \{ x, y, z \}} + 1 \right) \\
\frac{1}{2} \left(\frac{t}{\text{max} \; \{ x, y, z \}} + 1 \right)
\end{bmatrix}$$

This may be rewritten as

\begin{align*}
r & = \frac{1}{2} \cdot (\text{max} \; \{ x, y, z \})^{-1} \\
x' & = s \cdot r + \frac{1}{2} \\
y' & = t \cdot r + \frac{1}{2} \\
\end{align*}

$r$ is computed by `CUBEFACE1`, `FRCP.f32`, and `FMA.f32`. $x', y'$ are each computed as `FMA.f32`. To workaround numerical issues, it's additionally required to clamp $x', y'$ to $[0, 1]$; this saturation is free as an output modifier on the `FMA.f32` instructions.

Once the face index and coordinates are computed, they must be packed into Valhall's _Cube Map Descriptor_:

Table: Cube Map Descriptor

Bits      Value
--------  ------
0-28      S coordinate
29-31     Face index
32-63     T coordinate

Storing only the bottom 29-bits of the 32-bit floating point S coordinate suffices, since the hardware may infer the top 3-bits given the range restrictions of the output of the cube map transform.

The Cube Map Descriptor may be packed efficiently with a bitwise `MUX.i32` instruction, which acts as a per-bit conditional select. By fixing the mask of bits that the S coordinates occupies, the bottom word may be constructed by muxing between the S coordinate and the face index. The upper word requires no packing.

Putting it together, we get the full code sequence for the cubeface transform:

```nasm
CUBEFACE1.f32     m, x, y, z
CUBEFACE2.f32     f, x, y, z
FRCP.f32          n, m
FMA.f32           r, n, 0.5, -0.0
CUBE_SSEL.f32     s, z, x, f
CUBE_TSEL.f32     t, y, z, f
FMA.f32.clamp_0_1 x', s, r, 0.5
FMA.f32.clamp_0_1 y', t, r, 0.5
MUX.i32           x', x', f, #0x1FFFFFFF
```

# Register file

Like Bifrost, Valhall has 64 registers available, each 32-bits. There is a
trade off between register usage and thread count:

Register usage Threads
-------------- -------
$\leq 32$      Full
$> 32$         Half

Due to the hardware preloading some state in high registers, the register file
is discontiguous with full threads: the 32 available registers are $[R0, R15]$
and $[R48, R63]$.

Access to the register file has a power cost. Previous Mali generations
provided "temporary" or "bypass" registers, to pass data within a clause or
VLIW bundle without touching the register file. Valhall does not have
architecturally-visible temporary registers, as it lacks clauses or VLIW
bundles. Instead, Valhall passes register access through a forwarding buffer,
offering a dynamic alternative to static temporaries. Although this mechanism
is managed in hardware, most instruction sources have a `.discard` hint
indicating they WILL NOT be used in any later instructions. The `.discard` hint
indicates the hardware MAY evict the referenced register from the forwarding
buffer. The register's value after a discard is undefined and should not be
read. As short-hand, the discard hint is indicated in Mesa's disassembler by
prefixing register sources with a backtick \`.

Programmatic blending is implemented via blend shaders, which use the following
ABI:

Table: ABI for blend shaders

Register  Value
--------  ------
R0        Colour component #0
R1        Colour component #1
R2        Colour component #2
R3        Colour component #3
R52       Stack pointer, low word
R53       Stack pointer, high word
R54       Return address, low word

The stack pointer may be calculated by adding the amount of stack used by the
caller (the fragment shader) to the base thread local storage pointer, with a
sequence like:

```nasm
IADD_IMM.i32.ts r52, tls_ptr, #0x10
MOV.i32.ts r53, tls_ptr.hi
```

The return address may be calculated by adding the current program counter with
the length of an instruction (8) times the number of instructions to the next
`BLEND` intsruction minus 1. Then when the blend shader jumps back to this
address (plus a single instruction), it will blend the next render target.

```nasm
IADD_IMM.i32.id r54, program_counter, #0x8
```

The final render target should set `r54` to zero to terminate execution early.

This interface improves on Bifrost's handling of blend shaders by:

* Enabling both the blend shader and the fragment shader to use the stack
* Allowing position-independent blend shaders without passing return offsets in a side channel.

The following registers may be preloaded with hardware state by setting the
appropriate flags in the preload descriptor:

Table: Preloaded registers in vertex shaders

Register  Value
--------  ------
R59       Linear (unfolded) ID
R60       Vertex ID
R61       Instance ID

Table: Preloaded registers in fragment shaders

Register  Value
--------  ------
R58       Facingness, bottom bit set for front facing
R59       Fragment coordinates X/Y packed as 16-bit integers
R60       Sample coverage mask
R61       Sample coverage mask input in lower half, sample ID in byte 3

Table: Preloaded registers in compute shaders

Register  Value
--------  ------
R55       Local invocation ID (dimensions #0 and #1, low/high halves)
R56       Local invocation ID (dimension #2, low half)
R57       Work group ID (dimension #0)
R58       Work group ID (dimension #1)
R59       Work group ID (dimension #2)
R60       Global invocation ID (dimension #0)
R61       Global invocation ID (dimension #1)
R62       Global invocation ID (dimension #2)

# Transcendental operations

Transcendental operations on Valhall match Bifrost, using the same building
block instructions pieced together by the compiler in the same way. Common
operations are explained below. See Mesa's Bifrost compiler as a reference.

## Exponentials

Valhall provides fast computation of base-2 exponents with the `FEXP2.f32`
insturction. However, there is a catch: it requires its input in as an 8:24
fixed-point. The floating-point itself is passed as a second argument and used
only for numerical compliance in special cases.

Converting a 32-bit floating-point input to 8:24 fixed-point may be done by
multiplying by $2^{24}$ (adding $24$ to the exponent via `RSCALE`) and
converting to integer. The full code sequence to compute $y = 2^x$ is therefore

```nasm
RSCALE.f32 scaled, x, #24
F32_TO_S32 fixed, scaled
FEXP2.f32 y, fixed, x
```

To compute arbitrary exponents, recall the identity

$$b^x = \left( 2^{\log_2(b)} \right) ^x = 2^{x \cdot \log_2(b)}$$

For constant base, the log-2 of the base may be precomputed. Naively computing
exponents this way would require an extra multiplication, but we may make two
simplifications:

* Valhall has a four-source `FMA_RSCALE.f32` instruction performing a multiply-add in its first three sources and an exponent adjust given by the fourth.
* Multiplying by $2^{24}$ does not affect the special cases of $\exp_2$, so we may choose to pass the scaled argument to `FEXP2.f32` instead of the original one, allowing us to fuse the multiply.

Putting it together gives the code for other bases:

```
FMA_RSCALE.f32 scaled, x, #log2(base), #0.neg, #24
F32_TO_S32 fixed, scaled
FEXP2.f32 y, fixed, scaled
```

## Sine and cosine

For $\sin, \cos$, Valhall contains coarse lookup tables accessible with the
`FSIN_TABLE.u6, FCOS_TABLE.u6` instructions. These instructions multiply the
bottom 6-bits of their input by $\pi / 32$ and return the resulting $\sin$ or
$\cos$ value. They are used to calculate $\sin, \cos$ via a Taylor
approximation:

\begin{align*}
f(x + e) & = f(x) + e f'(x) + \frac{e^2}{2} f''(x) \\
\sin(x + e) & = \sin(x) + e \cos(x) - \frac{e^2}{2} \sin(x) \\
\cos(x + e) & = \cos(x) - e \sin(x) - \frac{e^2}{2} \cos(x) \\
\end{align*}

As a numerical trick, we introduce the magic constant `0x49400000`, with the
curious property that -- when interpreted as a floating-point and added to a
floating-point value $x \cdot \frac{2}{\pi}$ -- approximately equals
$\frac{32}{\pi} (x \mod 2 \pi)$ in the bottom 6-bits. This allows the domain
transformation to be done as a single fused floating-point multiply-add with
the magic constants.

As one more trick, we use the special `FMA_RSCALE.f32` instruction, which acts
like `FMA.f32` in its first three sources but biases the exponent by the value
of the fourth source. Recalling that a floating-point is encoded as $m \cdot
2^e$ for mantissa $m$ and exponent $e$, we may divide by two by biasing the
exponent by $-1$, as $\frac{1}{2} (m \cdot 2^e) = m \cdot 2^{e - 1}$.

Note that we require four magic constants that are not directly encodable.
These constants must be lowered to FAU slots. No FAU slot is needed for the
first addition, however, as the `FADD_IMM.f32` operation is available.

Putting it together gives a code sequence for $\sin$:

```
FMA.f32 x_u6, x, #TWO_OVER_PI, #SINCOS_BIAS
FADD_IMM.f32 temp, x_u6, #-SINCOS_BIAS
FMA.f32 e, temp, #MPI_OVER_TWO, x
FSIN_TABLE.u6 sinx, x_u6
FCOS_TABLE.u6 cosx, x_u6
FMA_RSCALE.f32 e2_over_2, e, e, -0.0, #-1
FMA.f32 quadratic, e2_over_2.neg, sinx, -0.0
FMA.f32.m1_1 temp2, e, cosx, quadratic
FADD.f32 dst, temp2, sinx
```

and similarly for $\cos$:

```
FMA.f32 x_u6, x, #TWO_OVER_PI, #SINCOS_BIAS
FADD.f32 temp, x_u6, #-SINCOS_BIAS
FMA.f32 e, temp, #MPI_OVER_TWO, x
FSIN_TABLE.u6 sinx, x_u6
FCOS_TABLE.u6 cosx, x_u6
FMA_RSCALE.f32 e2_over_2, e, e, -0.0, #-1
FMA.f32 quadratic, e2_over_2.neg, cosx, -0.0
FMA.f32.m1_1 temp2, e, sinx.neg, quadratic
FADD.f32 dst, temp2, cosx
```

# Sources

Regular sources are specified as an 8-bit structure.

Name | Bits
---- | ----
Value | 0:5
Mode | 6:7

Mode is an enumeration with the following values.

Index | Name
----  | ----
0 | Register
1 | Register with discard
2 | Uniform
3 | Immediate

Value is the index of 32-bit register in a register mode, the index of a 32-bit
uniform in uniform mode, or an index into an immediate table in an immediate
mode. The discard mode is used to discard the register after reading.

# Destinations

Regular destinations are specified as an 8-bit structure.

Name | Bits
---- | ----
Register | 0:5
Write low half | 6
Write high half | 7

The write low/high acts as a 16-bit write mask. At least one half MUST be
written in instruction. Therefore, instructions with no destinations still
require a placeholder where the destination otherwise would be; this should be
the constant `0xC0`.

# Staging registers

Message-passing instructions have special register access requirements compared
to arithmetic instructions. In particular, they can write multiple subsequent
registers, and they may need to reuse a register base as both a read and write
due to encoding limitations. These instructions do not use the regular
source/destination mechanism, and instead use the following 8-bit staging
register structure.

Name | Bits
---- | ----
Register | 0:5
Read | 6
Write | 7

The read/write flags specify the direction of transfer; both may be set
simultaneously, but at least one MUST be set. In a few cases, both are set even
when transferring only in a single logical direction. In a few other cases,
only the first 6-bits are stored, and the access flags are implied.

If multiple subsequent staging registers are accessed, the base must be aligned
to 2. However, even if 4 registers are accessed, it is not necessary to align to
4, only to 2. This restriction allows the hardware to use a 64-bit data path
without handling unaligned access, which is more efficient. This restriction
does not apply if only a single register is accessed.

# Instruction metadata

The last 7-bits of every instruction contains metadata for the instruction,
playing a similar role to the Bifrost clause header.

Name | Bits
---- | ----
Immediate mode | 0:1
Action | 2:5
Action mode | 6
_Reserved_ | 7

The immediate mode is an enumeration controlling how immediates sources are
interpreted. Leaving it zero allows the instruction to access the immediate
table, but it can be set to access various state of the shader core. See the
corresponding enumerations.

If the action mode bit is set, then action is an enumeration controlling
machine behaviour around the instruction's execution. If the action mode bit is
clear, then action is a bitfield specifying dependencies to wait on before
executing the _next_ instruction.

Name | Bits
---- | ----
Wait on slot #0 | 0
Wait on slot #1 | 1
Wait on slot #2 | 2

# Uniform/constant restrictions

Valhall imposes restrictions on access to uniforms and immediates in order to
simplify the hardware.

* An instruction may access no more than a single 64-bit uniform slot.
* An instruction may access no more than 64-bits of combined uniforms and constants.
* An instruction may only access uniforms in the default immediate mode.
* An instruction may access no more than a single special immediate (e.g. `lane_id`).

As Valhall is encoded regularly, it is possible to assemble programs exceeding
these limits. Such programs are invalid; nevertheless, implementations MAY
execute them with UNDEFINED results. Mali G78 will execute such an instruction
without faulting, but will select a *single* 64-bit slot corresponding to the
maximum uniform index and will overwrite the looked up result, producing
incorrect results. These limitations enable hardware optimizations.

These limits are inherited from Bifrost, where both constants and uniforms are
explicitly loaded to a "fast access uniform (FAU)" port, capable of loading a single
64-bit value every two instructions. An instruction like `FMA.f32 r2, r2, u4,
u7` cannot be encoded, since the uniforms come from different 64-bit slots. In
this situation, the Bifrost compiler must reorder the uniforms or insert a move
to solve the encoding hazard. Bifrost's FAU-RAM hardware is reused on Valhall,
even though the hardware limitations are no longer implicit in the encoding.

For example, to compile `fma(fma(a, #b, #c), #b, #d)`, the constants must be
lowered to uniforms. The optimal compile requires duplicating `#b` to two
separate uniforms:

```nasm
FMA.f32 r2, r2, u0, u1
FMA.f32 r2, r2, u2, u3
```

For another example, the program `FMA.f32 r0, u4, 0x0, u5` is invalid and will
produce incorrect results. The following are legal versions which will function
correctly:

```nasm
MOV.i32 r0, u5
FMA.f32 r0, u4, 0x0, r0
```

```nasm
MOV.i32 r0, 0x0
FMA.f32 r0, u4, r0, u5
```

In practice, the combined limit for constants and uniforms is rarely hit if
pre-shaders are used in tandem with constant folding. However, the limit is
visible on shaders that use a four-source conditional select. The optimal
compile of `a > #b ? #c : #d` with unique constants require two instructions:

```nasm
IADD_IMM.i32 r1, 0x0, #b
CSEL.f32.gt r0, a, r1, #c, #d
```

# Varyings

Unlike older Mali designs, the Valhall hardware allocates memory for varyings
automatically. The address of the allocated memory is available as `LEA_ATTR`.
It is stored to with standard `STORE` instructions with the appropriate segment
(position or varying).

Valhall supports a black hole address. Writes to any address with the top bit
set are discarded without faulting. The canonical black hole address is `BH =
(1 << 63)`. Black hole addresses are useful in the construction for point size
writes from a position shader:

```c
address = is_writing_points ? LEA_ATTR(r59 + 4) : (1 << 63)
STORE.i16.pos @psiz, address
```

Point sizes should be clamped to hardware bounds $[\frac{1}{2}, 1024]$.

\pagebreak
