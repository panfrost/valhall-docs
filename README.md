# Valhall documentation

This repository contains the public Valhall specification. Valhall is the microarchitecture implemented in the Mali G78 processor.

The docmuentation itself is licensed under the Creative Commons Attribution Share-Alike 4.0 International license (`SPDX-CC-BY-SA-4.0`). However it is a derivative work of `ISA.xml`, a machine-readable architecture reference available under the permissive MIT license for interoperability with Mesa.

## Building

The documentation may be built with the included Makefile, which uses pandoc and LuaLaTeX.
